
#include "stdafx.h"
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include<sstream>
using namespace std;


// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"

int __cdecl main(int argc, char **argv)
{
	//C:\\Users\\trieveonc\\Desktop\\MIS.txt
	string file;
	getline(cin, file);
	char ch;
	ifstream in(file.c_str());
	string filepath = "";
	while (in.get(ch)) {
		
		filepath += ch;	
	}

	char *data = new char[filepath.size() + 1];
	data[filepath.size()] = 0;
	memcpy(data, filepath.c_str(), filepath.size());



	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;
	char *sendbuf = "this is a test going to send a file and shit";
	char recvbuf[DEFAULT_BUFLEN];
	int iResult;
	int recvbuflen = DEFAULT_BUFLEN;

	// Validate the parameters
	

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 4;
	}
	


	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	iResult = getaddrinfo(argv[1], DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 2;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return 3;
		}

		// Connect to server.
		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		
		}
		break;
	}
	
	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server!\n");
		int x;
		cin >> x;
		WSACleanup();
		return 9;
	}

	// Send an initial buffer
	iResult = send(ConnectSocket, data, (int)strlen(data), 0);
	
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 1;
	}

	printf("Bytes Sent: %ld\n", iResult);
	int y;
	cin >> y;
	// shutdown the connection since no more data will be sent
	iResult = shutdown(ConnectSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		printf("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		WSACleanup();
		return 1;
	}

	// Receive until the peer closes the connection
	do {

		iResult = recv(ConnectSocket, recvbuf, recvbuflen,0);
		if (iResult > 0) {
			printf("Bytes received: %d\n", iResult);
			string sep = recvbuf;
			//if (sep[recvbuflen] == 'x') {
			printf("inhere");
			ofstream ofs;
			ofs.open("C:\\Users\\trieveonc\\Desktop\\test.txt", ofstream::out | ofstream::app);

			ofs << " more lorem ipsum";
			ofs.write(recvbuf,recvbuflen);

			ofs.close();
			//printf(recvbuf);
			
		}
		else if (iResult == 0)
			printf("Connection closed\n");
		else if (iResult == -1)
			printf("fk");
		else
			printf("recv failed with error: %d\n", WSAGetLastError());

	} while (iResult > 0);

	int x;
	cin >> x;
	// cleanup
	closesocket(ConnectSocket);
	WSACleanup();
	
	return 0;
}

