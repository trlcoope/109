// MisServer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <iostream>
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <process.h>
#include "FuncHandler.h"
#include  "Var.h"
#include "Numeric.h"
#include "Type.h"
#include "Real.h"
#include "Char.h"
#include "String.h"
#include <vector>
#include "Label.h"
using namespace std;

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
// #pragma comment (lib, "Mswsock.lib")

#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"
static int counter = 0;
unsigned __stdcall ClientSession(void *data)
{
	Var<string> v;
	int x;
	cout << "THanks for connecting to the server  " << _threadid;
	
	SOCKET *ClientSocket = (SOCKET*)&data;

	
	// Accept a client socket
	WSADATA wsaData;
	int iResult;
	int clientcount = 0;
	SOCKET ListenSocket = INVALID_SOCKET;
	//SOCKET ClientSocket = INVALID_SOCKET;
	

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	int iSendResult;
	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;
	
	cout << "client has connected";
	if (*ClientSocket == INVALID_SOCKET) {
		printf("accept failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		
		return 1;
	}
	cout << "client has connected";

	// No longer need server socket
	//	closesocket(ListenSocket);

	// Receive until the peer shuts down the connection
	do {
		iResult = recv(*ClientSocket, recvbuf, recvbuflen, 0);
		if (iResult > 0) {
			//printf("Bytes received: %d\n", iResult);
			
			string x = recvbuf;
			string s;
			cout << "prointgsize : " << s.size() <<"\n";
			int i = 0;
			char ch;
			vector<string> list;


			for (int i = 0; i < x.size(); i++) {
				
				if (x[i] != ' ') {
					if (x[i] == '$') {
						list.push_back(s);
						s = "";
					}
					else if (x[i] == ',') {

						list.push_back(s);
						s = "";
					}
					else {
						s += x[i];

					}

				}
			}

			list.push_back(s);
		  
			//cout << list[3];
			std::string line;
			vector<string> list2;
			vector<Label> labels;
			//cout << "length of the list" << list.size();
			bool b = true;
			int j;
			int jmpbk = 0;
			bool jmp = false;
			for (int i = 0; i < list.size(); i++)
				cout << "the list  "<< list[i] << "\n";
			
			cout <<"the list size  "<< list.size() << "\n";
			for (int i = 0; i < list.size(); i++) {
				cout << "Printing how the value of iiiiiii" << list[i] << "  iterator : " << i << "\n";
				if (list[i].find("VAR") != string::npos) {
					
					if (list[i + 2] == "NUMERIC") {
						cout << list[i];
						cout << list[i + 1];
						cout << list[i + 2];
						cout << list[i + 3];

						FuncHandler<Numeric<int>> a;
						Numeric<int> b = a.var(list[i + 1], list[i + 2], list[i + 3], "");
						v.numVars.push_back(b);

					}

					else if (list[i + 2] == "CHAR") {
						FuncHandler<Char<char>> b;
						Char<char> c = b.var(list[i + 1], list[i + 2], list[i + 3], "");
						v.charVars.push_back(c);
					}

					else if (list[i + 2] == "REAL") {
						FuncHandler<Real<double>> c;
						Real<double> b = c.var(list[i + 1], list[i + 2], list[i + 3], "");
						v.realVars.push_back(b);
					}
					else if (list[i + 2] == "STRING") {
						FuncHandler<String<string>> d;
						String<string> s = d.var(list[i + 1], list[i + 2], list[i + 3], list[i + 4]);
						v.stringVars.push_back(s);
					}


				}
				else if (list[i].find("ADD") != string::npos) {
					cout << " inside the add function";
					string a = list[i + 1];
					int x = i + 2;
					int y = i + 2;
					int value = 0;
					int count = 0;
					cout << list[i + 1] << " comparing  " << list[x] << "\n";

					while (list[i + 1] != list[y]) {
						count += 1;
						y++;

					}
					cout << "the count is :" << count;
					for (int i = 0; i < count - 1; i++) {
						cout << "Checking the value in the add function" << list[x] << " ayee\n";

						//cout << list[x];

						value += stoi(list[x]);
						x++;

					}
					cout << "The addition values are  " << value;
					Numeric<int> n;
					n.name = list[i + 1];
					n.val = value;
					v.numVars.push_back(n);

				}
				else if (list[i].find("MUL") != string::npos) {
					cout << " inside the mul function";
					string a = list[i + 1];
					int x = i + 2;
					int y = i + 2;
					int value = 1;
					int count = 0;
					cout << list[i + 1] << " comparing  " << list[x] << "\n";

					while (list[i + 1] != list[y]) {
						count += 1;
						y++;

					}
					cout << "the count is :" << count;
					for (int i = 0; i < count - 1; i++) {
						cout << "Checking the value in the mul function" << list[x] << " ayee\n";


						cout << "value of value" << value;
						value *= stoi(list[x]);
						x++;

					}
					cout << "The multiplication values are  " << value;
					Numeric<int> n;
					n.name = list[i + 1];
					n.val = value;
					v.numVars.push_back(n);

				}
				else if (list[i].find("SUB") != string::npos) {
					cout << " inside the sub function";
					string a = list[i + 1];
					int x = i + 2;
					int y = i + 2;
					int value = 1;
					int count = 0;
					cout << list[i + 1] << " comparing  " << list[x] << "\n";

					value = stoi(list[i + 3]) - stoi(list[i + 2]);

					cout << "The subtract values are  " << value;
					Numeric<int> n;
					n.name = list[i + 1];
					n.val = value;
					v.numVars.push_back(n);

				}
				else if (list[i].find("DIV") != string::npos) {
					cout << " inside the div function";
					string a = list[i + 1];
					int x = i + 2;
					int y = i + 2;
					int value = 1;
					int count = 0;
					cout << list[i + 1] << " comparing  " << list[x] << "\n";

					value = stoi(list[i + 3]) / stoi(list[i + 2]);

					cout << "The DIV values are  " << value;
					Numeric<int> n;
					n.name = list[i + 1];
					n.val = value;
					v.numVars.push_back(n);

				}
				else if (list[i].find("ASSIGN") != string::npos) {
					int pos = 0;
					int store2 = 0;
					for (int n = 0; n < v.numVars.size(); n++) {
						if (v.numVars[n].name == list[i + 1]) {
							cout << "found it";
							pos = n;
						}
						if (v.numVars[n].name == list[i + 2]) {
							cout << "found it 2";
							store2 = v.numVars[n].val;
						}

					}
					v.numVars[pos].val = store2;
					//replace(v.numVars.begin(), v.numVars.end(), stoi(list[i+3]), stoi(list[i+2]));


				}
				else if (list[i].find("SET_STR_CHAR") != string::npos) {
					cout << "we in here----  " << v.stringVars[0].name <<"\n";
					cout << "we in here----  " << stoi(list[i + 2]) << "A\n";
				
					for (int n = 0; n < v.stringVars.size(); n++) {
						cout << "The name :" << v.stringVars[n].name << "/n";
						if (list[i + 1] == v.stringVars[n].name) {
							cout << "we love gold";
							cout << "we in here----  " << v.stringVars[n].output.length() << "A\n";
							if (stoi(list[i + 2]) <= v.stringVars[n].output.length()) { // checking noted area on Assignment
								v.stringVars[n].output[stoi(list[i + 2])] = list[i + 3][1];
							}
							else {
								cout << "Throw error  at SET_STR";
							}
							cout << v.stringVars[n].output;
						}
					}

				}
				else if (list[i].find("GET_STR_CHAR") != string::npos) {
					cout << "we in here----  " << v.stringVars[0].name << "\n";
					cout << "we in here----  " << stoi(list[i + 2]) << "A\n";

					for (int n = 0; n < v.stringVars.size(); n++) {
						cout << "The name :" << v.stringVars[n].name << "/n";
						if (list[i + 1] == v.stringVars[n].name) {
							cout << "we love gold";
							cout << "we in here----  " << v.stringVars[n].output << "\n";
							if (stoi(list[i + 2]) <= v.stringVars[n].output.length()) { // checking noted area on Assignment
								char tmp = v.stringVars[n].output[2];
								v.stringVars[n].output[2] = v.stringVars[n].output[3];
								v.stringVars[n].output[3] = tmp;
								cout << "we in here p2----  " << v.stringVars[n].output << "\n";
							}
							else {
								cout << "Throw error  at SET_STR";
							}
							cout << v.stringVars[n].output;
						}
					}

				}
				else if (list[i].find("LABEL") != string::npos) {

					string s = list[i];
					cout << " the alue  of  " << s;
					string x = s.substr(6, s.length());
					cout << "this is the value offff xxxxxxxxxxxxxxx!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!@@@@ "<<x;
				    Label l(x, i);
					cout << l.pos;
					labels.push_back(l);
				}
				else if (list[i].find("JMP") != string::npos) {
					cout << "HDSALSDFKDJSIKRNFDSGNLDKSNGLSGN";
					if (list[i].find("JMPNZ") != string::npos) { 
						cout << "sdasd!!00000"; 

					}
					else if (list[i].find("JMPZ") != string::npos) {
						for (int n = 0; n < v.numVars.size(); n++) {
							cout << "@@@@@@@@@@" << v.numVars[n].val;
							cout << "@@@@@@@@@@" << list[i+2];

							if (v.numVars[n].name == list[i + 2])
								cout << "################ " << v.numVars[n].val;
							if (v.numVars[n].val == 0) {
								for (int j = 0; j < labels.size(); j++) {
									string x = list[i].substr(5, 9);
									cout << "String @@@@@@@@@@@@@@@@@@@@@@@@@@@  " << x;
									if (labels[j].name == x) {

										i = labels[n].pos;
									}
								}
							}

							else {
								cout << "not jumping anywhere";
							}

					}
					}else {

					cout << "JUMPING backkkkkkkkkkkkkkkk some steps";
					string s = list[i];
					cout << " the alue  of  " << s;
					string x = s.substr(4, s.length());
					cout << "this is the value offff xxxxxxxxxxxxxxx!!!!!!!!!!!!!!!@@@@ " << x;
					for (int n = 0; n < labels.size(); n++) {
						cout << " the label name " << labels[n].name;
						cout << " the jumps name  " << x;
						if (labels[n].name == x) {
							
							i = labels[n].pos;
						}
					}
					}
				}
			
			

			//	printf("Client count is : %d\n", clientcount);
				clientcount += 1;
				//printf(stoi(recvbuflen));
				// Echo the buffer back to the sender
				iSendResult = send(*ClientSocket, recvbuf, iResult, 0);
				if (iSendResult == SOCKET_ERROR) {
					printf("send failed with error: %d\n", WSAGetLastError());
					closesocket(*ClientSocket);
					WSACleanup();
					return 1;
				}
				
				printf("Bytes sent: %d\n", iSendResult);
			}
		}
		else if (iResult == 0)
			printf("Connection closing...\n");
		else {
			printf("recv failed with error: %d\n", WSAGetLastError());
			closesocket(*ClientSocket);
			WSACleanup();
			return 2;
		}

	} while (iResult > 0);
	

	// shutdown the connection since we're done
	iResult = shutdown(*ClientSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		printf("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(*ClientSocket);
		WSACleanup();
		return 1;
	}

	// cleanup
//	int x = 0;
	counter++;
	cin >> x;
	closesocket(*ClientSocket);
	
	WSACleanup();

	return 12;
}
int __cdecl main(void)
{
	WSADATA wsaData;
	int iResult;
	int clientcount = 0;
	SOCKET ListenSocket = INVALID_SOCKET;
	SOCKET ClientSocket = INVALID_SOCKET;

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	int iSendResult;
	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;


	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Create a SOCKET for connecting to server
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}
	cout << "server isss on";
	// Setup the TCP listening socket
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	freeaddrinfo(result);

	iResult = listen(ListenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}


	while ((ClientSocket = accept(ListenSocket, NULL, NULL))) {
		// Create a new thread for the accepted client (also pass the accepted client socket).
		unsigned threadID;

		HANDLE hThread = (HANDLE)_beginthreadex(NULL, 0, &ClientSession,(void*)ClientSocket, 0, &threadID);
		
		
	}
}


