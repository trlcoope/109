#pragma once
#include "stdafx.h"
#include <string>
#include <vector>
#include "Char.h"
#include "Numeric.h"
#include "Real.h"
#include "String.h"

using namespace std;

template<typename t>
class Var
{
public:
	vector<Numeric<int>> numVars;
	vector<Real<double>> realVars;
	vector<String<string>> stringVars;
	vector<Char<char>> charVars;
	Var();
	
};
template<typename T>
Var<T>::Var() {

}
